import * as mongoose from 'mongoose';
import { Schema } from 'mongoose';
import { isEmail } from 'validator';
import { UserDocument } from './user.document';
import ObjectId = mongoose.Schema.Types.ObjectId;

export const EmbeddedFavoriteItemSchema = new Schema({
  itemId: {
    type: ObjectId
  },
  priceLimit: {
    type: Number
  }
});

export const UserSchema = new Schema({
  username: {
    type: String
  },
  email: {
    type: String,
    validate: [isEmail, 'invalid email'],
    required: true,
    unique: true
  },
  firstName: {
    type: String
  },
  lastName: {
    type: String
  },
  birthDate: {
    type: Date
  },
  password: {
    type: String,
    required: true
  },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  enabled: {
    type: Boolean,
    default: true
  },
  emailing: {
    type: Boolean,
    default: true
  },
  loginEnabled: {
    type: Boolean,
    default: true
  },
  roles: {
    type: [ObjectId],
    default: []
  },
  lastLogIn: {
    type: Date
  },
  favoritesItems: {
    type: [EmbeddedFavoriteItemSchema],
    default: []
  },
  savedRecipes: {
    type: [ObjectId],
    default: []
  }
});

UserSchema.pre('save', function(next) {
  (this as UserDocument).updatedAt = new Date();

  next();
});
