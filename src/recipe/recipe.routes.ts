import * as express from 'express';
import { Validator } from 'express-json-validator-middleware';
import { RecipeController } from './recipe.controller';
import { RouterManager } from '../utils/router.manager';
import { internalAuthenticationMiddleware } from '../middlewares/internal-authentication.middleware';
import { internalAuthorizationMiddleware } from '../middlewares/internal-authorization.middleware';
import { RecipeCreateSchema, RecipeQuerySchema, RecipeUpdateSchema } from './recipe.schema';

const router: express.Router = express.Router();
const validator = new Validator({ allErrors: true, removeAdditional: true });
const recipeController = new RecipeController();

const routerManager = new RouterManager(router);
const resource = 'recipes';

/**
 * @apiDefine Recipe Recipe
 *
 * List of endpoints for managing recipes.
 */
routerManager
  .route('/recipes')
  /**
   * @api {get} /recipes Get recipe list
   *
   * @apiGroup Recipe
   *
   * @apiSuccess {array} recipes
   *
   * @apiSuccessExample {json} Success response
   *     HTTP/1.1 200 OK
   *     [{
   *        "_id": "5b179f629fea4000ffcf2fbc",
   *        "resource": "user",
   *        "action":["find"]
   *    }]
   */
  .get({
    handlers: [internalAuthenticationMiddleware, internalAuthorizationMiddleware, recipeController.getAll],
    resource,
    action: 'read'
  })
  /**
   * @api {post} /recipes Create recipe
   *
   * @apiGroup Recipe
   *
   * @apiParam {String} name,
   *
   * @apiSuccess {String} name
   * @apiSuccess {String} id ObjectId
   *
   * @apiSuccessExample {json} Success response
   *     HTTP/1.1 201 Created
   *     {
   *        "_id": "5b179f629fea4000ffcf2fbc",
   *        "resource": "name"
   *    }
   */
  .post({
    handlers: [
      validator.validate({
        body: RecipeCreateSchema
      }),
      internalAuthenticationMiddleware,
      internalAuthorizationMiddleware,
      recipeController.create
    ],
    resource,
    action: 'create'
  });

routerManager.route('/recipes/search').get({
  handlers: [internalAuthenticationMiddleware, internalAuthorizationMiddleware, recipeController.search],
  resource,
  action: 'read'
});

routerManager.route('/recipes/:id/auctionator/:aid').get({
  handlers: [internalAuthenticationMiddleware, internalAuthorizationMiddleware, recipeController.auctionator],
  resource,
  action: 'read'
});
routerManager
  .route('/recipes/:id')
  /**
   * @api {get} /recipes/:id Get recipe
   *
   * @apiGroup Recipe
   *
   * @apiParam {String} id ObjectId
   *
   * @apiSuccess {String} name
   * @apiSuccess {String} id ObjectId
   *
   * @apiSuccessExample {json} Success response
   *     HTTP/1.1 200 OK
   *     {
   *        "_id": "5b179f629fea4000ffcf2fbc",
   *        "resource": "user",
   *        "action":["find"]
   *    }
   *
   * @apiErrorExample {json} Error recipe not found
   *     HTTP/1.1 404 Not Found
   *     {
   *       "error": {
   *          "code": "ERRNOTFOUND",
   *          "message": "Not found"
   *       }
   *     }
   */
  .get({
    handlers: [
      validator.validate({
        params: RecipeQuerySchema
      }),
      internalAuthenticationMiddleware,
      internalAuthorizationMiddleware,
      recipeController.get
    ],
    resource,
    action: 'read'
  })
  .put({
    handlers: [
      validator.validate({
        body: RecipeUpdateSchema
      }),
      internalAuthenticationMiddleware,
      internalAuthorizationMiddleware,
      recipeController.update
    ],
    resource,
    action: 'update'
  });

export default router;
