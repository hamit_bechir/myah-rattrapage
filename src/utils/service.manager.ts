import { Document } from 'mongoose';
import { Service } from './service.abstract';

export class ServiceManager<T extends Service<Document>> {
  private readonly serviceByName: {
    [name: string]: T;
  };

  constructor() {
    this.serviceByName = {};
  }

  registerService(service: T) {
    this.serviceByName[service.name()] = service;
  }

  getService(name: string): T {
    return this.serviceByName[name];
  }
}

export const serviceManager = new ServiceManager();
